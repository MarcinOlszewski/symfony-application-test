<?php

namespace App\Controller;

use App\Entity\Product;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

/**
 * Class ProductController
 * @package App\Controller
 */
final class ProductController extends AbstractFOSRestController{

    /**
     * @param $log
     * @throws \Exception
     */
    public function logger($log){
        $logger = new Logger('product');
        $logger->pushHandler(new StreamHandler(__DIR__.'app.log', Logger::DEBUG));
        $logger->info($log);
    }


    /**
     * @Rest\Get("/products")
     *
     * @return View
     */
    public function getProducts(): View{

        $products = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findAll();

        $this->logger('Searching for all products');

        if($products){
            return View::create($products, Response::HTTP_OK);
        }
        else{
            return View::create('There is nothing to show', Response::HTTP_NO_CONTENT);
        }

    }

    /**
     * @Rest\Get("/product/{id}")
     *
     * @return View
     */
    public function getProduct($id): View{
        $product = $this->getDoctrine()
            ->getRepository(Product::class)
            ->find($id);
        if($product){
            $this->logger('Succesfully searching for product with id = ' .$id);
            return View::create($product, Response::HTTP_OK);
        }
        else{
            $this->logger('Unsuccesfully searching for product with id = ' .$id);
            return View::create('Unable to find product with this ID', Response::HTTP_BAD_REQUEST);
        }

    }


    /**
     * @Rest\Post("/product")
     * @param Request $request
     * @return View
     * @throws \Exception
     */
    public function postCreateProduct(Request $request): View{
        $product = $this->createProduct($request);
        $this->saveProduct($product);
        return View::create('Product created', Response::HTTP_CREATED);
    }

    /**
     * @Rest\Put("/product/{id}")
     * @param Request $request
     * @return View
     * @throws \Exception
     */
    public function putEditProduct($id, Request $request): View{
        $product = $this->putProduct($id, $request);
        $this->saveProduct($product);
        return View::create('Product editted succesfulyy', Response::HTTP_CREATED);
    }

    /**
     * @param Request $request
     * @return Product
     * @throws \Exception
     */
    public function createProduct(Request $request): Product{
        $product = new Product();
        $product->setName($request->get('name'));
        $product->setAmount($request->get('amount'));
        $product->setEan($request->get('ean'));
        $product->setUpdatedate(new \DateTime);

        $this->logger('Created an product');

        return $product;
    }

    /**
     * @param Product $product
     */
    public function saveProduct(Product $product): void{
        $productEntityManager = $this->getDoctrine()->getManager();
        $productEntityManager->persist($product);
        $productEntityManager->flush();
    }

    /**
     * @param Request $request
     * @return Product
     * @throws \Exception
     */
    public function putProduct($id, Request $request): Product{
        $product = $this->getDoctrine()
            ->getRepository(Product::class)
            ->find($id);
        if($product){
            $product->setName($request->get('name'));
            $product->setAmount($request->get('amount'));
            $product->setEan($request->get('ean'));
            $product->setUpdatedate(new \DateTime);

            $this->logger('Updated product with id = ' .$id . ' using PUT method');
        }
        return $product;
    }

    /**
     * @param $id
     * @Rest\Delete("/product/{id}")
     * @return View
     * @throws \Exception
     */
    public function deleteProduct($id){
        $product = $this->getDoctrine()
            ->getRepository(Product::class)
            ->find($id);
        if($product){
            $EntityManager = $this->getDoctrine()->getManager();
            $EntityManager->remove($product);
            $EntityManager->flush();

            $this->logger('Deleted an product with id = ' . $id);
        }

        return View::create('Product deleted', Response::HTTP_NO_CONTENT);
    }

}
