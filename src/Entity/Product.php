<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use http\Env\Response;


/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $amount;

    /**
     * @ORM\Column(type="text", length=13)
     */
    private $ean;

    /**
     * @ORM\Column(type="date")
     */
    private $updatedate;

    public function getId(): ?int{
        return $this->id;
    }

    public function getName(): ?string{
        return $this->name;
    }

    public function setName(string $name): self{
        $this->name = $name;

        return $this;
    }

    public function getAmount(): ?int{
        return $this->amount;
    }

    public function setAmount(int $amount): self{
        $this->amount = $amount;

        return $this;
    }

    public function getEan(): ?string{
        return $this->ean;
    }

    public function setEan(string $ean): self{
        if(\strlen($ean) != 13){
            throw new \InvalidArgumentException('Ean code needs to be 13 length numeric code.');
        }

        if(is_numeric($ean)){
            $this->ean = $ean;
        }
        else{
            throw new \InvalidArgumentException('Ean code needs to be numeric, use only numbers.');
        }
        return $this;
    }

    public function getUpdatedate(){
        return $this->updatedate;
    }

    public function setUpdatedate($updatedate): self{
        $this->updatedate = $updatedate;

        return $this;
    }
}
